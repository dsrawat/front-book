const intialCart = [];

export const cartReducer = (state, action) => {
    const { type, payload } = action;
    switch (type) {
        case 'ADD_BOOK_TO_CART':
            {
                const [book] = state.filter(book => book.id === payload.id);
                if (!book) {
                    return ([...state, { ...payload, quantity: 1 }])
                }
                return state;
            }
        case 'REMOVE_BOOK_FROM_CART':
            {
                const CART_ITEMS = state.filter(book => book.id !== payload.id);
                return [...CART_ITEMS];
            }
        case 'INCREMENT_BOOK':
            {
                return state.map(book => {
                    if (book.id === payload.id) book.quantity++;
                    return book;
                })
            }
        case 'DECREMENT_BOOK':
            {
                const [target_book] = state.filter(book => book.id === payload.id);
                if (target_book.quantity > 1) {
                    return state.map(book => {
                        if (book.id === payload.id) book.quantity--;
                        return book;
                    })
                }
                const CART_ITEMS = state.filter(book => book.id !== payload.id);
                return [...CART_ITEMS];
            }
        case 'RESET':
            return intialCart;
        default:
            return state
    }
}