function matchSting(str = "", subStr = "") {
    return str.toLowerCase().includes(subStr.toLowerCase())
}


export const filterReducer = (bookList, action) => {
    const { filterType, filterContent, data } = action;
    switch (filterType) {
        case 'all':
            return data.filter(book =>
                matchSting(book.title, filterContent) || matchSting(book.author, filterContent)
                || matchSting(book.category, filterContent)
            );

        case 'title':
            return data.filter(book => matchSting(book.title, filterContent));

        case 'author':
            return data.filter(book => matchSting(book.author, filterContent));

        case 'category':
            return data.filter(book => matchSting(book.category, filterContent));

        case 'price':
            return data.filter(book => book.price <= filterContent);

        default:
            return data;
    }
}