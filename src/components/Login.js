import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { postReq } from '../__utilities/__axios';
import __server_apis from '../__utilities/__server_apis';

export default function Login(props) {

    const { loggedIn, setLoggedIn } = props.loggedInState;
    const [logMsg, setLogMsg] = useState('');
    const { username, setUsername } = props.usernameState;

    const [userDetails, setUserDetails] = useState({
        username: "",
        password: "",
    })
    const onChangeHandler = e => {
        setUserDetails({ ...userDetails, [e.target.name]: e.target.value });
    }

    const submitHandler = e => {
        e.preventDefault();

        postReq(__server_apis.login, {
            "username": userDetails.username,
            "password": userDetails.password
        })
            .then(res => {
                localStorage.setItem('jwt', res.data.accessToken);
                localStorage.setItem('role')
                setUsername(userDetails.username);
                setLogMsg('Succesfully Logged In. Redirecting to Home Page ...');
                setTimeout(() => {
                    setLoggedIn(true);
                }, 1000);
            })
            .catch(err => {
                setLogMsg('Something Went Wrong...');
            });
    }

    return (
        <div className="login">
            <form onSubmit={submitHandler}>
                <div className="form-title">Login Here</div>
                <hr />
                <label htmlFor="username">User Name</label>
                <input className="form-input" type="text" name="username" placeholder="username" required onChange={onChangeHandler} />
                <label htmlFor="password">Password</label>
                <input className="form-input" type="password" name="password" placeholder="password" required onChange={onChangeHandler} />
                <input className="form-submit" type="submit" value="Login" />
            </form>
            <Link to="/register"><li>New User? Register here</li></Link>
            <p style={{ color: 'gray' }}>{logMsg}</p>
            {loggedIn ? <Redirect to="/" /> : null}
        </div>
    )
}
