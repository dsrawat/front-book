import React, { useState, useEffect } from 'react';
import book_img from '../images/book_img.jpg';
import { getReq } from '../__utilities/__axios';
import __server_apis from '../__utilities/__server_apis';

export default function BookDetail({ match, cartContext }) {

    const [added_to_cart_msg, setAdded_to_cart_msg] = useState('');
    const { cartDispatch } = cartContext;
    const bookId = match.params.id;
    const [book, setBook] = useState({})
    useEffect(() => {
        getReq(`${__server_apis.getBookById}${bookId}`).then(res => setBook(res.data))
    }, [])

    return (
        <div className="book-detail-card">
            <div className="book-card">

                <img src={book_img} alt="Book" />

                <div className="book-container">
                    <p>Title : {book.title}</p>
                    <p>Author : {book.author}</p>
                    <p>Price : {book.price}</p>
                    <p>Category : {book.category}</p>
                    <p>Description : {book.description}</p>
                    <button onClick={handleOnClick}>Add To Cart</button>
                    {added_to_cart_msg ? <p style={{ color: 'gray' }}>{added_to_cart_msg}</p> : null}

                </div>

            </div>
        </div>
    )

    function handleOnClick() {
        setAdded_to_cart_msg('Product Added To The Cart');
        cartDispatch({
            type: 'ADD_BOOK_TO_CART',
            payload: book
        })
    }
}
