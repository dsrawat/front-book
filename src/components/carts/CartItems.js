import React from 'react'

export default function CartItems({ book, cartContext }) {
    const { cartDispatch } = cartContext;

    return (
        <React.Fragment>
            <hr />
            <span>{book.title} &nbsp; {book.price}  &emsp; </span>
            <button className="button-cart"
                onClick={() => cartDispatch({
                    type: 'INCREMENT_BOOK',
                    payload: book
                })} >+</button>
            {book.quantity}
            <button className="button-cart" onClick={() => cartDispatch({
                type: 'DECREMENT_BOOK',
                payload: book
            })} >-</button>
            &ensp;
            <button onClick={() => cartDispatch({
                type: 'REMOVE_BOOK_FROM_CART',
                payload: book
            })} >Delete</button>
            <hr />

        </React.Fragment>
    )
}
