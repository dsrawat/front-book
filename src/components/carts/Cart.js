import React, { useState, useEffect } from 'react';
import CartItems from './CartItems';
import { postReq } from '../../__utilities/__axios';
import __server_apis from '../../__utilities/__server_apis';

export default function Cart(props) {

    const { cartState, cartDispatch } = props.cartContext;
    const [orderMsg, setOrderMsg] = useState('');
    const { username, setUsername } = props.usernameState;

    return (
        <aside>
            <h2>{calculateCartTotal(cartState) ? ("Total : " + calculateCartTotal(cartState)) : "Cart is empty."}</h2>

            {
                cartState.map(cartItem =>
                    <CartItems key={cartItem.id} book={cartItem}
                        cartContext={props.cartContext} />)
            }

            {
                cartState.length ? <button onClick={handleOrder}> Order </button> : null
            }
            <div>{orderMsg}</div>

        </aside>
    )

    function calculateCartTotal(books) {
        return books.reduce((cartTotal, cartItem) =>
            cartTotal + (cartItem.price * cartItem.quantity), 0)
    }

    function handleOrder() {
        if (props.loggedIn) {
            postReq(__server_apis.postOrder,
                {
                    "username": username,
                    "orderDate": getOrderDate(),
                    "items": cartState.map(item => ({ "bookid": item.id, "quantity": item.quantity })),
                    "subtotal": calculateCartTotal(cartState)
                },
                {
                    headers: { 'Authorization': "Bearer " + localStorage.getItem('jwt') }
                }

            ).then(res => {
                console.log('success ::: ', res.data)
                setOrderMsg(`Order placed of ${calculateCartTotal(cartState)}.
                Thanks for Shopping with us...`);
                cartDispatch({ type: 'RESET' });
            })
                .catch(err => setOrderMsg('Something Went Wrong'));
        }
        else {
            setOrderMsg('Log In first to Order');
        }
    }

    function getOrderDate() {
        const today = new Date();
        return `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}`;
    }
}

