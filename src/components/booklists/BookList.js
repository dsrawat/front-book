import React, { useEffect, useReducer, useState } from 'react';
import Book from './Book';
// import { books } from '../data';
import { filterReducer } from '../../reducers/filterReducer';
import { getReq } from '../../__utilities/__axios';
import __server_apis from '../../__utilities/__server_apis';

export default function BookList({ filterSearchState }) {

    const { filterType, filterContent } = filterSearchState.filterSearch;
    const [filterReducerState, filterReducerDispatch] = useReducer(filterReducer, []);
    const [loading, setLoading] = useState(true);
    const [booksData, setBooksData] = useState([]);
    const [loadingMsg, setLoadingMsg] = useState('Loading');

    useEffect(() => {
        getReq(__server_apis.getBookList).then(res => {
            setBooksData(res.data);
            setLoading(false);
            setLoadingMsg('');
        }
        ).catch(err => setLoadingMsg('Something Went Wrong!'));
    }, []
    )

    useEffect(() => {
        filterReducerDispatch({ filterType, filterContent, data: booksData })
    }, [filterSearchState.filterSearch, booksData]);

    return (
        <div className="book-list">
            {
                (!loading) ? filterReducerState.map(book => <Book key={book.id} book={book} />) :
                    <p style={{ color: 'gray' }}>{loadingMsg}</p>

            }
        </div>
    )
}
