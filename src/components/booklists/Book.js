import React from 'react';
import { Link } from 'react-router-dom';
import book_img from '../../images/book_img.jpg';

export default function Book(props) {
    const { id, title, author, price, category } = props.book;
    return (
        <div className="book-card">
            <img src={book_img} alt="Book" />
            <div className="book-container">
                <Link to={`/book/${id}`}><p>{title}</p></Link>
                <p>Author : {author}</p>
                <p>Price : {price}</p>
                <p>category : {category}</p>
            </div>
        </div>
    )
}
