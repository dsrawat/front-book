import React from 'react';
import { Link } from 'react-router-dom';

export default function NavCart(props) {

    return (
        <div className="nav-cart">
            <Link to="/cart"> <i className="fa" style={{ fontSize: 35 }}>&#xf07a;</i>
                <span>{props.totalCartCount ? props.totalCartCount : ""}</span></Link>
        </div>
    )
}
