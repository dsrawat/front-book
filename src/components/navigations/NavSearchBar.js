import React from 'react';
// import search from '../../images/search.png';

export default function NavSearchBar(props) {

    const { filterSearch, setFilterSearch } = props.filterSearchState;

    function submitHandler(e) {
        e.preventDefault();
        document.getElementById('d-searchbar').value = "";
        //Change it to filter state changer
    }

    return (
        <form className="nav-search-bar" onSubmit={submitHandler}>

            <select className="form-select" name="filterType" value={filterSearch.filterType}
                onChange={e => setFilterSearch({ ...filterSearch, filterType: e.target.value })}>
                <option>all</option>
                <option>title</option>
                <option>author</option>
                <option>category</option>
                <option>price</option>
            </select>

            <input className="form-input" id="d-searchbar" type="text" name="searchbar" value={filterSearch.filterContent}
                onChange={(e) => setFilterSearch({ ...filterSearch, filterContent: e.target.value })} />
            <button className="form-submit" type="submit"><i className="fa fa-search"></i></button>
        </form>
    )
}
