import React from 'react';
import { Link } from 'react-router-dom';
import NavSearchBar from './NavSearchBar';
import NavCart from './NavCart';
import NavSiteLogo from './NavSiteLogo';

export default function Navigation(props) {

    // Destructuring Props and Initializing Variables



    const { filterSearchState, totalCartCount, loggedInState } = props;
    const { loggedIn, setLoggedIn } = loggedInState;
    const { filterSearch, setFilterSearch } = filterSearchState;

    return (
        <nav>
            <NavSiteLogo setSearchBarToDefault={setSearchBarToDefault} />
            <NavSearchBar filterSearchState={filterSearchState} />
            <div className="nav-log">{formatLoggedOption()}</div>
            <NavCart totalCartCount={totalCartCount} />
        </nav>
    )

    function formatLoggedOption() {
        return loggedIn ?
            <div onClick={handleLogout}><Link to="/">logout</Link></div>
            :
            <div onClick={setSearchBarToDefault}><Link to="/login">Login</Link></div>;
    }

    function handleLogout() {
        localStorage.removeItem('jwt');
        setLoggedIn(false);
    }

    function setSearchBarToDefault() {
        setFilterSearch({ filterType: "all", filterContent: "" })
    }
}
