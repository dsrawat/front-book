import React from 'react';
import { Link } from 'react-router-dom';

export default function NavSiteLogo(props) {
    return (
        <div onClick={props.setSearchBarToDefault} className="nav-site-logo">
            <Link to="/">TechnoGram Book Store</Link>
        </div>
    )
}
