import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { postReq } from '../__utilities/__axios';
import __server_apis from '../__utilities/__server_apis';

export default function Register() {

    const [userDetails, setUserDetails] = useState({
        name: "",
        password: "",
        username: "",
        contact: "",
        address: "",
        role: "user"
    })

    const [registerMsg, setRegisterMsg] = useState('');
    const [isRegistered, setIsRegistered] = useState(false);

    const onChangeHandler = event => {
        setUserDetails({ ...userDetails, [event.target.name]: event.target.value });
    }

    const submitHandler = event => {
        event.preventDefault();
        postReq(__server_apis.register, userDetails)
            .then(res => {
                setRegisterMsg('Successfully Registered. Redirecting to login page...')
                setTimeout(() => setIsRegistered(true), 1000);
            })
            .catch(err => setRegisterMsg('Something Went Wrong!'));

    }

    return (
        <div className="register">
            <div className="form-title">Register Here</div>
            <hr />
            <form onSubmit={submitHandler}>
                <label htmlFor="username">User Name</label>
                <input className="form-input" type="text" name="username" placeholder="username" required onChange={onChangeHandler} />
                <label htmlFor="name">Name</label>
                <input className="form-input" type="text" name="name" placeholder="name" required onChange={onChangeHandler} />
                <label htmlFor="address">Address</label>
                <input className="form-input" type="text" name="address" placeholder="address" onChange={onChangeHandler} />
                <label htmlFor="contact">Contact</label>
                <input className="form-input" type="text" name="contact" placeholder="contact" required onChange={onChangeHandler} />
                <label htmlFor="password">Password</label>
                <input className="form-input" type="password" name="password" placeholder="password" required onChange={onChangeHandler} />
                <input className="form-submit" type="submit" value="register" />
            </form>
            <p style={{ color: 'gray' }}>{registerMsg}</p>
            {isRegistered ? <Redirect to="/login" /> : null}
        </div>
    )
}



