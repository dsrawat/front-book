import React, { useState, useReducer, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Register from './components/Register';
import Login from './components/Login';
import BookList from './components/booklists/BookList';
import Cart from './components/carts/Cart';
import Navigation from './components/navigations/Navigation';
import BookDetail from './components/BookDetail';
import { cartReducer } from './reducers/cartReducer';


export const cartContext = React.createContext();

export default function App() {
    

    const [cart, dispatch] = useReducer(cartReducer, []);
    const [ username, setUsername ] = useState('');
    const [filterSearch, setFilterSearch] = useState({ filterType: "all", filterContent: "" });
    const [loggedIn, setLoggedIn] = useState(false);

    console.log('app-check',username)

    useEffect(() => {
        if (localStorage.getItem('jwt')) setLoggedIn(true)
        else setLoggedIn(false)
    }, [loggedIn])

    console.log("calling");
    return (
        <div className="app">
            <Router>
                <header>
                    <Navigation totalCartCount={getCartTotal()}
                        filterSearchState={{ filterSearch, setFilterSearch }}
                        loggedInState={{ loggedIn, setLoggedIn }} />
                </header>

                <main >
                    <Route exact path="/"
                        render={props => <BookList {...props}
                            filterSearchState={{ filterSearch, setFilterSearch }}
                        />} />


                    <Route exact path="/login"
                        render={props => <Login {...props}
                            loggedInState={{ loggedIn, setLoggedIn }}
                            usernameState={{username, setUsername}}
                        />}
                    />


                    <Route exact path="/register" component={Register} />

                    <Route path="/book/:id"
                        render={props => <BookDetail {...props}
                            cartContext={{ cartState: cart, cartDispatch: dispatch }}
                        />}
                    />

                    <Route path="/cart"
                        render={props => <Cart {...props}
                            cartContext={{ cartState: cart, cartDispatch: dispatch }}
                            loggedIn={loggedIn}
                            usernameState={{username, setUsername}}
                        />}
                    />


                </main>

            </Router>
        </div>
    );

    function getCartTotal() {
        return cart.reduce((totalItems, curr) => totalItems + curr.quantity, 0);
    }

}

// ReactDOM.render(
//     <Route exact path="/">
//       {loggedIn ? <Redirect to="/dashboard" /> : <PublicHomePage />}
//     </Route>,
//     node
//   )
