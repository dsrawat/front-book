const __server_apis = {
    "getBookList": "http://localhost:8085/store/book/getlist",
    "getBookById": "http://localhost:8085/store/book/search/BookId/",
    "login": "http://localhost:8085/login",
    "register": "http://localhost:8085/users/sign-up",
    "postOrder": "http://localhost:8085/store/order/"
}
export default __server_apis;